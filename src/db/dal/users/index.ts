import { IUserInput, IUserOuput } from "../../../db/models/User";

export interface IUserRepository {
    create (userInput: IUserInput): Promise<IUserOuput>
    getUserByEmail (email: string): Promise<IUserOuput | undefined> 
}