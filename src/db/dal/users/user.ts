import { injectable } from 'inversify'
import  { IUserRepository } from '.'
import { IUserInput, IUserOuput, User } from "../../models/User";

@injectable()
export class UserRepository implements IUserRepository {
  public create(userInput: IUserInput): Promise<IUserOuput> {
    return User.create(userInput)
  }
  public async getUserByEmail(email: string): Promise<IUserOuput | undefined> {
    const user: any = await User.findOne({
    where: {
      email
    }
   })

   return user ? user : undefined;   
  }
}
