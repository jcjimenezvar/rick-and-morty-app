export interface IGetAllUsersFilters {
  isDeleted?: boolean;
  includeDeleted?: boolean;
}
