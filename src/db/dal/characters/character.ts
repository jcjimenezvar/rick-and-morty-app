import { injectable } from 'inversify'
import { Op } from 'sequelize'
import { ICharacterRepository } from '.'
import {
  Character,
  ICharacterInput,
  ICharacterOuput,
} from '../../models/Character'

@injectable()
export class CharacterRepository implements ICharacterRepository {
  public async getAll(params: any): Promise<ICharacterOuput[]> {
    const filterBy: string =
      params && params.filterBy ? params.filterBy : undefined
    const value: any = params && params.value ? params.value : undefined
    const limit: any = params && params.limit ? params.limit : 10
    const offset: any = params && params.offset ? params.offset : 0
    let options = {}
    let query: any = {
      status: true,
    }
    options = { ...options, ...query }
    switch (filterBy) {
      case 'gender':
        query = {
          gender: value,
        }
        break
      case 'species':
        query = {
          species: value,
        }
        break
      case 'character_status':
        query = {
          character_status: value,
        }
        break
      case 'location':
        query = {
          location: {
            [Op.like]: `%${value}%`,
          },
        }
        break
      case 'episode':
        query = {
          episode: {
            [Op.contains]: [value],
          },
        }
        break

      default:
        query = {
          status: true,
        }
        break
    }
    options = { ...options, ...query }

    return Character.findAll({
      limit,
      offset,
      raw: true,
      where: options,
    })
  }
  public async findByCharacterId(
    characterId: number
  ): Promise<ICharacterOuput | null> {
    return Character.findOne({
      raw: true,
      where: {
        characterId,
      },
    })
  }

  public async updateCharacterStatus(
    characterId: number,
    status: boolean
  ): Promise<[number]> {
    return Character.update(
      {
        status,
      },
      {
        where: {
          characterId,
        },
      }
    )
  }

  public async createCharacter(
    character: ICharacterInput
  ): Promise<ICharacterOuput> {
    return Character.create(character)
  }
}
