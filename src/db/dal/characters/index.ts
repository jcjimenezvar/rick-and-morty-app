import { ICharacterInput, ICharacterOuput } from 'src/db/models/Character'

export interface ICharacterRepository {
  getAll(filters: any): Promise<ICharacterOuput[]>
  findByCharacterId(characterId: number): Promise<ICharacterOuput | null>
  updateCharacterStatus(characterId: number, status: boolean): Promise<[number]>
  createCharacter(character: ICharacterInput): Promise<ICharacterOuput>
}
