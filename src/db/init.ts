import { Character } from './models/Character'
import { User } from './models/User'
const isDev = process.env.NODE_ENV === 'development'

const dbInit = () => {
  User.sync({ alter: isDev })
  Character.sync({ alter: isDev })
}
export default dbInit
