import { DataTypes, Model, Optional } from "sequelize";
import config from "../config";

interface IUserAttributes {
  id: number;
  name?: string;
  email?: string;
  password?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
export interface IUserInput extends Optional<IUserAttributes, "id"> {}
export interface IUserOuput extends Required<IUserAttributes> {}

export class User
  extends Model<IUserAttributes, IUserInput>
  implements IUserAttributes
{
  public id!: number;
  public name!: string;
  public email!: string;
  public password!: string;

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public readonly deletedAt!: Date;
}

User.init(
  {
    email: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },    
    password: {
      allowNull: false,
      type: DataTypes.STRING,
    },
  },
  {
    sequelize: config,
    tableName: 'users',
    timestamps: true,
    underscored: true,
  }
);
