import { DataTypes, Model, Optional } from 'sequelize'
import config from '../config'

interface ICharacterAttributes {
  characterId?: number
  characterStatus?: string
  createdAt?: Date
  deletedAt?: Date
  episode?: string[]
  gender?: string
  id?: number
  location?: string
  name?: string
  species?: string
  status?: boolean
  updatedAt?: Date
}
export interface ICharacterInput extends Optional<ICharacterAttributes, 'id'> {}
export interface ICharacterOuput extends Required<ICharacterAttributes> {}

export class Character extends Model<ICharacterAttributes, ICharacterInput>
  implements ICharacterAttributes {
  public characterId!: number
  public characterStatus!: string
  public episode!: string[]
  public gender!: string
  public id!: number
  public location!: string
  public name!: string
  public species!: string
  public status!: boolean

  // timestamps!
  public readonly createdAt!: Date
  public readonly updatedAt!: Date
  public readonly deletedAt!: Date
}

Character.init(
  {
    characterId: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    characterStatus: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    episode: {
      allowNull: false,
      type: DataTypes.ARRAY(DataTypes.STRING),
    },
    gender: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    location: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    species: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    status: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
    },
  },
  {
    sequelize: config,
    tableName: 'characters',
    timestamps: true,
    underscored: true,
  }
)
