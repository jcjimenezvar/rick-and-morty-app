import 'reflect-metadata'
import { UserService } from './userService'
import { UserRepository } from '../../../db/dal/users/user'

jest.mock('../../../db/dal/users/user')
jest.mock('sequelize')

describe('characterService', () => {
  beforeEach(() => {
    process.env.NODE_ENV = 'development'
    process.env.NODE_PORT = '4000'
    process.env.ROOT_PATH = '/app'
    process.env.DB_NAME = 'rickandmorty'
    process.env.DB_USER = 'admin'
    process.env.DB_HOST = 'localhost'
    process.env.DB_PORT = '5432'
    process.env.DB_DRIVER = 'postgres'
    process.env.DB_PASSWORD = 'admin'
    process.env.JWT_SECRET = 'rick@ndm0rty$3cr3t'
    process.env.SALT_ROUNDS = '10'
  })
  afterEach(() => {
    jest.clearAllMocks()
  })

  const userRepository = new UserRepository()
  const userService = new UserService(userRepository)
  test('userService is instanced correctly', () => {
    expect(userService).toBeDefined()
  })

  test('userService create user', async () => {
    userRepository.create = jest.fn().mockImplementationOnce(() =>
      Promise.resolve({
        id: 1,
        name: 'Juan',
        email: 'correo@correo.com',
        password: 'encrypted password',
      })
    )

    try {
      const response = await userService.create({
        name: 'Juan',
        email: 'correo@correo.com',
        password: 'pre password',
      })
      expect(response).toBeDefined()
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('userService get user by email user', async () => {
    userRepository.getUserByEmail = jest.fn().mockImplementationOnce(() =>
      Promise.resolve({
        id: 1,
        name: 'Juan',
        email: 'correo@correo.com',
        password: 'encrypted password',
      })
    )

    try {
      const response = await userService.getUserByEmail('correo@correo.com')
      expect(response).toBeDefined()
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })
})
