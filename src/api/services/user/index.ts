import { IUserInput, IUserOuput } from "../../../db/models/User";

export interface IUserService {
    create (userInput: IUserInput): Promise<IUserOuput>
    getUserByEmail (email: string): Promise<IUserOuput | undefined> 
}