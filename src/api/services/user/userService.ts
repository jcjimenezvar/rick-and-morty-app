import { inject, injectable } from "inversify";
import { IUserService } from ".";
import { TYPES } from "../../../config/ioc/types";
import { IUserRepository } from "../../../db/dal/users";
import { IUserInput, IUserOuput } from "../../../db/models/User";

@injectable()
export class UserService implements IUserService {
  constructor(
    @inject(TYPES.IUserRepository) private userDal: IUserRepository,
  ) {}
  
  public create(userInput: IUserInput): Promise<IUserOuput> {
    return this.userDal.create(userInput);
  }

  public getUserByEmail(email: string): Promise<IUserOuput | undefined>  {
    return this.userDal.getUserByEmail(email);
  }
}
