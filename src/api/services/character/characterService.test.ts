import 'reflect-metadata'
import { IConfig } from '../../../config/vars'
import { CharacterService } from './characterService'
import { AxiosConsumer } from '../../consumer/axiosConsumer'
import { CharacterRepository } from '../../../db/dal/characters/character'

jest.mock('../../consumer/axiosConsumer')
jest.mock('../../../db/dal/characters/character')
jest.mock('../../../config/vars')
jest.mock('sequelize')

describe('characterService', () => {
  beforeEach(() => {
    process.env.NODE_ENV = 'development'
    process.env.NODE_PORT = '4000'
    process.env.ROOT_PATH = '/app'
    process.env.DB_NAME = 'rickandmorty'
    process.env.DB_USER = 'admin'
    process.env.DB_HOST = 'localhost'
    process.env.DB_PORT = '5432'
    process.env.DB_DRIVER = 'postgres'
    process.env.DB_PASSWORD = 'admin'
    process.env.JWT_SECRET = 'rick@ndm0rty$3cr3t'
    process.env.SALT_ROUNDS = '10'
  })
  afterEach(() => {
    jest.clearAllMocks()
  })
  class MockConfig implements IConfig {
    public getVars = () => {
      return {
        jwtSecret: 'secret',
        externalService: {
          url: 'http://myurl.com',
        },
      }
    }
  }

  const config = new MockConfig()
  const axiosConsumer = new AxiosConsumer()
  const characterRepository = new CharacterRepository()
  const characterService = new CharacterService(
    config,
    axiosConsumer,
    characterRepository
  )
  test('characterService is instanced correctly', () => {
    expect(characterService).toBeDefined()
  })

  test('characterService create character', async () => {
    axiosConsumer.get = jest.fn().mockImplementationOnce(() =>
      Promise.resolve({
        id: 1,
        episode: ['http://episode/1'],
        gender: 'Male',
        location: {
          name: 'location',
        },
        name: 'character name',
        species: 'species',
        status: 'Alive',
      })
    )
    characterRepository.findByCharacterId = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve(null))
    characterRepository.createCharacter = jest.fn().mockImplementationOnce(() =>
      Promise.resolve({
        id: 1,
        status: 'Alive',
        episodes: [],
        gender: 'Male',
      })
    )
    try {
      const response = await characterService.saveFavoriteCharacter(2)
      expect(response).toBeDefined()
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('characterService id y lower than one', async () => {
    try {
      const response = await characterService.saveFavoriteCharacter(0)
      expect(response).toBeUndefined()
    } catch (error) {
      expect(error).toBeDefined()
    }
  })

  test('characterService id y upper than 826', async () => {
    try {
      const response = await characterService.saveFavoriteCharacter(827)
      expect(response).toBeUndefined()
    } catch (error) {
      expect(error).toBeDefined()
    }
  })

  test('characterService does not exists', async () => {
    axiosConsumer.get = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve(undefined))
    try {
      const response = await characterService.saveFavoriteCharacter(2)
      expect(response).toBeUndefined()
    } catch (error) {
      expect(error).toBeDefined()
    }
  })

  test('characterService character already exists update status to true', async () => {
    axiosConsumer.get = jest.fn().mockImplementationOnce(() =>
      Promise.resolve({
        id: 1,
        episode: ['http://episode/1'],
        gender: 'Male',
        location: {
          name: 'location',
        },
        name: 'character name',
        species: 'species',
        status: 'Alive',
      })
    )
    characterRepository.findByCharacterId = jest
      .fn()
      .mockImplementationOnce(() =>
        Promise.resolve({
          id: 2,
          status: false,
        })
      )
    characterRepository.updateCharacterStatus = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve([1]))
    try {
      const response = await characterService.saveFavoriteCharacter(2)
      expect(response).toBeDefined()
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('characterService character already exists update status to false', async () => {
    axiosConsumer.get = jest.fn().mockImplementationOnce(() =>
      Promise.resolve({
        id: 1,
        episode: ['http://episode/1'],
        gender: 'Male',
        location: {
          name: 'location',
        },
        name: 'character name',
        species: 'species',
        status: 'Alive',
      })
    )
    characterRepository.findByCharacterId = jest
      .fn()
      .mockImplementationOnce(() =>
        Promise.resolve({
          id: 2,
          status: true,
        })
      )
    characterRepository.updateCharacterStatus = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve([1]))
    try {
      const response = await characterService.saveFavoriteCharacter(2)
      expect(response).toBeDefined()
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('characterService character axios throws an error', async () => {
    axiosConsumer.get = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(new Error('Mocked error')))
    try {
      const response = await characterService.saveFavoriteCharacter(2)
      expect(response).toBeUndefined()
    } catch (error) {
      expect(error).toBeDefined()
    }
  })
})

describe('characterService', () => {
  beforeEach(() => {
    process.env.NODE_ENV = 'development'
    process.env.NODE_PORT = '4000'
    process.env.ROOT_PATH = '/app'
    process.env.DB_NAME = 'rickandmorty'
    process.env.DB_USER = 'admin'
    process.env.DB_HOST = 'localhost'
    process.env.DB_PORT = '5432'
    process.env.DB_DRIVER = 'postgres'
    process.env.DB_PASSWORD = 'admin'
    process.env.JWT_SECRET = 'rick@ndm0rty$3cr3t'
    process.env.SALT_ROUNDS = '10'
  })
  afterEach(() => {
    jest.clearAllMocks()
  })
  class MockConfig implements IConfig {
    public getVars = () => {
      return {
        jwtSecret: 'secret',
        externalService: {
          url: 'http://myurl.com',
        },
      }
    }
  }

  const config = new MockConfig()
  const axiosConsumer = new AxiosConsumer()
  const characterRepository = new CharacterRepository()
  const characterService = new CharacterService(
    config,
    axiosConsumer,
    characterRepository
  )
  test('characterService find all characters returns 200', async () => {
    characterRepository.getAll = jest.fn().mockImplementationOnce(() =>
      Promise.resolve([
        {
          id: 1,
        },
      ])
    )
    const params = {}
    try {
      const response = await characterService.getAllFavoriteCharacters(params)
      expect(response).toBeDefined()
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('characterService id y lower than one', async () => {
    try {
      const response = await characterService.saveFavoriteCharacter(0)
      expect(response).toBeUndefined()
    } catch (error) {
      expect(error).toBeDefined()
    }
  })

  test('characterService id y upper than 826', async () => {
    try {
      const response = await characterService.saveFavoriteCharacter(827)
      expect(response).toBeUndefined()
    } catch (error) {
      expect(error).toBeDefined()
    }
  })

  test('characterService does not exists', async () => {
    axiosConsumer.get = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve(undefined))
    try {
      const response = await characterService.saveFavoriteCharacter(2)
      expect(response).toBeUndefined()
    } catch (error) {
      expect(error).toBeDefined()
    }
  })

  test('characterService character already exists update status to true', async () => {
    axiosConsumer.get = jest.fn().mockImplementationOnce(() =>
      Promise.resolve({
        id: 1,
        episode: ['http://episode/1'],
        gender: 'Male',
        location: {
          name: 'location',
        },
        name: 'character name',
        species: 'species',
        status: 'Alive',
      })
    )
    characterRepository.findByCharacterId = jest
      .fn()
      .mockImplementationOnce(() =>
        Promise.resolve({
          id: 2,
          status: false,
        })
      )
    characterRepository.updateCharacterStatus = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve([1]))
    try {
      const response = await characterService.saveFavoriteCharacter(2)
      expect(response).toBeDefined()
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('characterService character already exists update status to false', async () => {
    axiosConsumer.get = jest.fn().mockImplementationOnce(() =>
      Promise.resolve({
        id: 1,
        episode: ['http://episode/1'],
        gender: 'Male',
        location: {
          name: 'location',
        },
        name: 'character name',
        species: 'species',
        status: 'Alive',
      })
    )
    characterRepository.findByCharacterId = jest
      .fn()
      .mockImplementationOnce(() =>
        Promise.resolve({
          id: 2,
          status: true,
        })
      )
    characterRepository.updateCharacterStatus = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve([1]))
    try {
      const response = await characterService.saveFavoriteCharacter(2)
      expect(response).toBeDefined()
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('characterService character axios throws an error', async () => {
    axiosConsumer.get = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(new Error('Mocked error')))
    try {
      const response = await characterService.saveFavoriteCharacter(2)
      expect(response).toBeUndefined()
    } catch (error) {
      expect(error).toBeDefined()
    }
  })
})
