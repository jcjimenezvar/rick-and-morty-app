import { inject, injectable } from 'inversify'
import { ICharacterService } from '.'
import { IAxiosConsumer } from '../../../api/consumer'
import { TYPES } from '../../../config/ioc/types'
import { IConfig } from '../../../config/vars'
import { ICharacterRepository } from '../../../db/dal/characters'
import { ICharacterInput, ICharacterOuput } from '../../../db/models/Character'

@injectable()
export class CharacterService implements ICharacterService {
  constructor(
    @inject(TYPES.IConfig)
    private config: IConfig,
    @inject(TYPES.IAxiosConsumer)
    private axiosConsumer: IAxiosConsumer,
    @inject(TYPES.ICharacterRepository)
    private characterRepository: ICharacterRepository
  ) {}

  public async saveFavoriteCharacter(id: number): Promise<any | undefined> {
    try {
      if (id < 1 || id > 826) {
        throw new Error(`There character id must be between 1 to 826: ${id}`)
      }
      const characterInformation: any = await this.getCharacterInformation(id)
      if (!characterInformation) {
        throw new Error(`There is not information for the id: ${id}`)
      }
      const character: ICharacterOuput | null = await this.characterExist(id)
      if (character && character !== null) {
        if (character.status) {
          character.status = false
        } else {
          character.status = true
        }

        const updated = await this.updateCharacterStatus(id, character.status)
        if (updated[0] === 1) {
          return {
            message: `The character with id ${id} already exists in the database. The status was changed to ${
              character.status ? 'Active' : 'Inactive'
            }`,
          }
        }
      }
      return this.createCharacter(characterInformation)
    } catch (error) {
      throw {
        message: error,
      }
    }
  }

  public async getAllFavoriteCharacters(
    params: any
  ): Promise<ICharacterOuput[]> {
    return this.characterRepository.getAll(params)
  }

  private async getCharacterInformation(id: number) {
    const serviceUrl = this.config.getVars().externalService.url

    return this.axiosConsumer.get(`${serviceUrl}/${id}`).then((result: any) => {
      if (!result) return undefined
      return {
        episodes: result.episode.map(
          (episode: string) => episode.split('/episode/')[1]
        ),
        gender: result.gender,
        id: result.id,
        location: result.location!.name,
        name: result.name,
        species: result.species,
        status: result.status,
      }
    })
  }

  private async characterExist(id: number): Promise<ICharacterOuput | null> {
    return this.characterRepository.findByCharacterId(id)
  }

  private async updateCharacterStatus(
    id: number,
    status: boolean
  ): Promise<[number]> {
    return this.characterRepository.updateCharacterStatus(id, status)
  }

  private async createCharacter(character: any): Promise<ICharacterOuput> {
    const newCharacterData: ICharacterInput = {
      characterId: character.id,
      characterStatus: character.status,
      episode: character.episodes,
      gender: character.gender,
      location: character.location,
      name: character.name,
      species: character.species,
      status: true,
    }
    return this.characterRepository.createCharacter(newCharacterData)
  }
}
