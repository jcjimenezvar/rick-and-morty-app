import { ICharacterOuput } from '../../../db/models/Character'

export interface ICharacterService {
  saveFavoriteCharacter(id: number): Promise<number | undefined>
  getAllFavoriteCharacters(params: any): Promise<ICharacterOuput[]>
}
