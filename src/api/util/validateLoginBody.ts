import { injectable } from 'inversify'
import { IValidateLoginBody } from '.'
import { IUserOuput } from '../../db/models/User'
import { loginSchema } from './schemas'

@injectable()
export class ValidateLoginBody implements IValidateLoginBody {
  public validate(data: IUserOuput) {
    return loginSchema.validate(data, { abortEarly: true })
  }
}
