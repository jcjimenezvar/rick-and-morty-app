import { inject, injectable } from 'inversify'
import * as jwt from 'jsonwebtoken'
import { IConfig } from 'src/config/vars'
import { IManageJWT } from '.'
import { TYPES } from '../../config/ioc/types'
import { IUserOuput } from '../../db/models/User'

@injectable()
export class ManageJWT implements IManageJWT {
  constructor(@inject(TYPES.IConfig) private config: IConfig) {}
  public sign(data: IUserOuput): string {
    return jwt.sign(JSON.stringify(data), this.config.getVars().jwtSecret)
  }

  public decode(token: string, userId: number): boolean {
    let authorized: boolean = false
    const decoded: any = jwt.verify(
      token.split('Bearer ')[1],
      this.config.getVars().jwtSecret
    )
    if (decoded.id === userId) authorized = true
    return authorized
  }
}
