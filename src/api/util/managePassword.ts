import * as bycript from 'bcrypt'
import { inject, injectable } from 'inversify'
import { IManagePassword } from '.'
import { TYPES } from '../../config/ioc/types'
import { IConfig } from '../../config/vars'

@injectable()
export class ManagePassword implements IManagePassword {
  constructor(@inject(TYPES.IConfig) private config: IConfig) {}
  public async encrypt(password: string): Promise<string> {
    const salt: string = await bycript.genSalt(this.config.getVars().saltRounds)
    return bycript.hash(password, salt)
  }

  public async compare(password: string, hash: string): Promise<boolean> {
    return bycript.compare(password, hash)
  }
}
