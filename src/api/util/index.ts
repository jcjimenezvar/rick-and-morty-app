import joi from 'joi'
import { IUserInput } from 'src/db/models/User'

export interface IValidateSignUpBody {
  validate(data: IUserInput): joi.ValidationResult
}

export interface IValidateLoginBody {
  validate(data: IUserInput): joi.ValidationResult
}

export interface IManagePassword {
  encrypt(password: string): Promise<string>
  compare(password: string, hashedPassword: string): Promise<boolean>
}

export interface IManageJWT {
  sign(data: IUserInput): string
  decode(token: string, userId: number): boolean
}
