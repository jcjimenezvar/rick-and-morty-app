import { injectable } from 'inversify'
import { IValidateSignUpBody } from '.'
import { IUserOuput } from '../../db/models/User'
import { signUpSchema } from './schemas'

@injectable()
export class ValidateSignUpBody implements IValidateSignUpBody {
  public validate(data: IUserOuput) {
    return signUpSchema.validate(data, { abortEarly: true })
  }
}
