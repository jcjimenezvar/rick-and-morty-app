import 'reflect-metadata'

import axios from 'axios'
import { AxiosConsumer } from './axiosConsumer'

jest.mock('axios')

describe('axiosConsumer', () => {
  const axiosConsumer = new AxiosConsumer()
  test('axiosConsumer is instanced correctly', async () => {
    expect(axiosConsumer).toBeDefined()
  })
  test('axiosConsumer return information without error', async () => {
    axios.get = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve({ data: { id: 1 } }))
    const response = await axiosConsumer.get('http://myservice.com/user')
    expect(response).toBeDefined()
  })
  test('axiosConsumer return error', async () => {
    axios.get = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(new Error('Mocked error')))
    try {
      const response = await axiosConsumer.get('http://myservice.com/user')
      expect(response).toBeUndefined()
    } catch (error) {
      expect(error).toBeDefined()
    }
  })
})
