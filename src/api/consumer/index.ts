export interface IAxiosConsumer{
    get(url: string): Promise<any>
}