import axios from 'axios'
import { injectable } from 'inversify'
import { IAxiosConsumer } from '.'

@injectable()
export class AxiosConsumer implements IAxiosConsumer {
  public async get(url: string): Promise<any> {
    return axios.get(url).then((data: any) => data.data)
  }
}
