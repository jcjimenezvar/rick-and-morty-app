import console from 'console'
import * as express from 'express'
import { inject } from 'inversify'
import {
  controller,
  httpPost,
  httpPut,
  interfaces,
  next,
  request,
  response,
} from 'inversify-express-utils'
import joi from 'joi'
import { TYPES } from '../../config/ioc/types'
import { IUserOuput } from '../../db/models/User'
import { IUserService } from '../services/user'
import {
  IManageJWT,
  IManagePassword,
  IValidateLoginBody,
  IValidateSignUpBody,
} from '../util'

@controller('/')
export class UserController implements interfaces.Controller {
  constructor(
    @inject(TYPES.IUserService)
    private userService: IUserService,
    @inject(TYPES.IValidateSignUpBody)
    private validateSignUpBody: IValidateSignUpBody,
    @inject(TYPES.IValidateLoginBody)
    private validateLoginBody: IValidateLoginBody,
    @inject(TYPES.IManagePassword)
    private managePassword: IManagePassword,
    @inject(TYPES.IManageJWT)
    private manageJWT: IManageJWT
  ) {}
  @httpPut('sign-up')
  public async createUser(
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    try {
      const data = req.body
      const result: joi.ValidationResult = this.validateSignUpBody.validate(
        data
      )
      if (result.error) {
        return res
          .status(400)
          .json({
            message: result.error.details[0].message,
          })
          .send()
      }

      const userAlreadySigned:
        | IUserOuput
        | undefined = await this.userService.getUserByEmail(data.email)

      if (userAlreadySigned) {
        return res
          .status(400)
          .json({
            message: `The user with email ${data.email} is already registered, please login`,
          })
          .send()
      }

      const hashedPassword: string = await this.managePassword.encrypt(
        data.password
      )
      data.password = hashedPassword

      const serviceResponse: IUserOuput = await this.userService.create(data)

      return res
        .status(201)
        .json(serviceResponse)
        .send()
    } catch (error) {
      return res
        .status(500)
        .json(error)
        .send()
    }
  }

  @httpPost('login')
  public async login(
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    try {
      const data = req.body
      const result: joi.ValidationResult = this.validateLoginBody.validate(data)
      if (result && result.error) {
        return res
          .status(400)
          .json({
            message: result.error.details[0].message,
          })
          .send()
      }
      const user:
        | IUserOuput
        | undefined = await this.userService.getUserByEmail(data.email)

      if (!user) {
        return res
          .status(404)
          .json({
            message: `The user with email ${data.email} is not registered`,
          })
          .send()
      }

      const isCorrect: boolean = await this.managePassword.compare(
        data.password,
        user.password
      )

      if (!isCorrect) {
        return res.status(400).json({
          message:
            'The information is not correct, please verify it and try again',
        })
      }

      return res
        .status(200)
        .json({
          accessToken: this.manageJWT.sign(user),
          message: `Hello ${user.name.toUpperCase()} you are logged correctly!!!`,
          userId: user.id,
        })
        .send()
    } catch (error) {
      return res
        .status(500)
        .json(error)
        .send()
    }
  }
}
