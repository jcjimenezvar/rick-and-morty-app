import 'reflect-metadata'
import { CharacterRepository } from '../../db/dal/characters/character'
import { AxiosConsumer } from '../consumer/axiosConsumer'
import { CharacterService } from '../services/character/characterService'
import { createRequest, createResponse } from 'node-mocks-http'
import { CharacterController } from './charactersController'
import { ManageJWT } from '../util/manageJWT'
import { IConfig } from '../../config/vars'

jest.mock('../services/character/characterService')
jest.mock('../consumer/axiosConsumer')
jest.mock('../../db/dal/characters/character')
jest.mock('../../config/vars/configService')
jest.mock('sequelize')

class MockConfig implements IConfig {
  public getVars = () => {
    return {
      jwtSecret: 'secret',
      externalService: {
        url: 'http://myurl.com'
      }
    }
  }
}

describe('charactersController', () => {
  beforeEach(() => {
    process.env.NODE_ENV = 'development'
    process.env.NODE_PORT = '4000'
    process.env.ROOT_PATH = '/app'
    process.env.DB_NAME = 'rickandmorty'
    process.env.DB_USER = 'admin'
    process.env.DB_HOST = 'localhost'
    process.env.DB_PORT = '5432'
    process.env.DB_DRIVER = 'postgres'
    process.env.DB_PASSWORD = 'admin'
    process.env.JWT_SECRET = 'rick@ndm0rty$3cr3t'
    process.env.SALT_ROUNDS = '10'
  })
  const config = new MockConfig()
  const axiosConsumer = new AxiosConsumer()
  const characterRepository = new CharacterRepository()
  const characterService = new CharacterService(
    config,
    axiosConsumer,
    characterRepository
  )
  const manageJWT = new ManageJWT(config)
  const characterController = new CharacterController(
    characterService,
    manageJWT
  )
  test('characterService is instanced correctly', async () => {
    expect(characterService).toBeDefined()
  })

  test('characterService PUT create character return 200', async () => {
    const req = createRequest({
      params: {
        id: 1,
      },
      headers: {
        authorization: 'Bearer fake token',
      },
      body: {
        userId: 1,
      },
    })
    const res = createResponse()
    manageJWT.decode = jest.fn().mockImplementationOnce(() => true)
    characterService.saveFavoriteCharacter = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve({ id: 1 }))

    try {
      await characterController.add(req, res)
      expect(res._getStatusCode()).toEqual(201)
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('characterService PUT create character return 500', async () => {
    const req = createRequest({
      params: {
        id: 1,
      },
      headers: {
        authorization: 'Bearer fake token',
      },
      body: {
        userId: 1,
      },
    })
    const res = createResponse()
    manageJWT.decode = jest.fn().mockImplementationOnce(() => true)
    characterService.saveFavoriteCharacter = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(new Error('Mocked Error')))

    try {
      await characterController.add(req, res)
    } catch (error) {
      expect(error).toBeDefined()
      expect(res._getStatusCode()).toEqual(500)
    }
  })

  test('characterService PUT create character return 401 id is not present', async () => {
    const req = createRequest({
      params: {},
      headers: {
        authorization: 'Bearer fake token',
      },
      body: {
        userId: 1,
      },
    })
    const res = createResponse()
    try {
      await characterController.add(req, res)
    } catch (error) {
      expect(error).toBeDefined()
      expect(res._getStatusCode()).toEqual(401)
    }
  })

  test('characterService PUT create character return 401 authorization is not present ', async () => {
    const req = createRequest({
      params: {
        id: 1,
      },
      headers: {},
      body: {
        userId: 1,
      },
    })
    const res = createResponse()
    try {
      await characterController.add(req, res)
    } catch (error) {
      expect(error).toBeDefined()
      expect(res._getStatusCode()).toEqual(401)
    }
  })

  test('characterService PUT create character return 401 token validation fails', async () => {
    const req = createRequest({
      params: {
        id: 1,
      },
      headers: {
        authorization: 'Bearer fake token',
      },
      body: {
        userId: 1,
      },
    })
    const res = createResponse()
    manageJWT.decode = jest.fn().mockImplementationOnce(() => false)
    try {
      await characterController.add(req, res)
    } catch (error) {
      expect(res._getStatusCode()).toEqual(401)
      expect(error).toBeDefined()
    }
  })
})

describe('charactersController', () => {
  beforeEach(() => {
    process.env.NODE_ENV = 'development'
    process.env.NODE_PORT = '4000'
    process.env.ROOT_PATH = '/app'
    process.env.DB_NAME = 'rickandmorty'
    process.env.DB_USER = 'admin'
    process.env.DB_HOST = 'localhost'
    process.env.DB_PORT = '5432'
    process.env.DB_DRIVER = 'postgres'
    process.env.DB_PASSWORD = 'admin'
    process.env.JWT_SECRET = 'rick@ndm0rty$3cr3t'
    process.env.SALT_ROUNDS = '10'
  })
  const config = new MockConfig()
  const axiosConsumer = new AxiosConsumer()
  const characterRepository = new CharacterRepository()
  const characterService = new CharacterService(
    config,
    axiosConsumer,
    characterRepository
  )
  const manageJWT = new ManageJWT(config)
  const characterController = new CharacterController(
    characterService,
    manageJWT
  )
  test('characterService GET get all characters return 200', async () => {
    const req = createRequest({
      headers: {
        authorization: 'Bearer fake token',
      },
      body: {
        userId: 1,
      },
    })
    const res = createResponse()
    manageJWT.decode = jest.fn().mockImplementationOnce(() => true)
    characterService.getAllFavoriteCharacters = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve([{ id: 1 }]))

    try {
      await characterController.get(req, res)
      expect(res._getStatusCode()).toEqual(200)
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('characterService GET get all characters return 500', async () => {
    const req = createRequest({
      headers: {
        authorization: 'Bearer fake token',
      },
      body: {
        userId: 1,
      },
    })
    const res = createResponse()
    manageJWT.decode = jest.fn().mockImplementationOnce(() => true)
    characterService.getAllFavoriteCharacters = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(new Error("Mocked Error")))

    try {
      await characterController.get(req, res)      
    } catch (error) {
      expect(res._getStatusCode()).toEqual(500)
      expect(error).toBeDefined()
    }
  })

  test('characterService GET get all characters return 401 no authorization present', async () => {
    const req = createRequest({
      headers: {},
      body: {
        userId: 1,
      },
    })
    const res = createResponse()
    try {
      await characterController.get(req, res)      
    } catch (error) {
      expect(res._getStatusCode()).toEqual(401)
      expect(error).toBeDefined()
    }
  })

  test('characterService GET get all characters return 401 no authorized token error', async () => {
    const req = createRequest({
      headers: {
        authorization: 'Bearer fake token'
      },
      body: {
        userId: 1,
      },
    })
    const res = createResponse()
    try {
      await characterController.get(req, res)      
    } catch (error) {
      expect(res._getStatusCode()).toEqual(401)
      expect(error).toBeDefined()
    }
  })

  test('characterService GET get all characters return 400 no found information', async () => {
    const req = createRequest({
      headers: {
        authorization: 'Bearer fake token'
      },
      body: {
        userId: 1,
      },
    })
    const res = createResponse()
    manageJWT.decode = jest.fn().mockImplementationOnce(() => true)
    characterService.getAllFavoriteCharacters = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve([]))
    try {
      await characterController.get(req, res)      
    } catch (error) {
      expect(res._getStatusCode()).toEqual(401)
      expect(error).toBeDefined()
    }
  })

})
