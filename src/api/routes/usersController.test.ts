import 'reflect-metadata'
import { UserRepository } from '../../db/dal/users/user'
import { UserService } from '../services/user/userService'
import { createRequest, createResponse } from 'node-mocks-http'
import { UserController } from './usersController'
import { ManageJWT } from '../util/manageJWT'
import { IConfig } from '../../config/vars'
import { ValidateSignUpBody } from '../util/validateSingUpBody'
import { ValidateLoginBody } from '../util/validateLoginBody'
import { ManagePassword } from '../util/managePassword'

jest.mock('../services/user/userService')
jest.mock('../../db/dal/users/user')
jest.mock('../../config/vars/configService')
jest.mock('../util/validateSingUpBody')
jest.mock('../util/validateLoginBody')
jest.mock('../util/managePassword')
jest.mock('../util/manageJWT')
jest.mock('sequelize')

class MockConfig implements IConfig {
  public getVars = () => {
    return {
      jwtSecret: 'secret',
      externalService: {
        url: 'http://myurl.com',
      },
    }
  }
}

describe('charactersController', () => {
  beforeEach(() => {
    process.env.NODE_ENV = 'development'
    process.env.NODE_PORT = '4000'
    process.env.ROOT_PATH = '/app'
    process.env.DB_NAME = 'rickandmorty'
    process.env.DB_USER = 'admin'
    process.env.DB_HOST = 'localhost'
    process.env.DB_PORT = '5432'
    process.env.DB_DRIVER = 'postgres'
    process.env.DB_PASSWORD = 'admin'
    process.env.JWT_SECRET = 'rick@ndm0rty$3cr3t'
    process.env.SALT_ROUNDS = '10'
  })

  afterEach(() => {
    jest.clearAllMocks()
  })
  const config = new MockConfig()
  const userRepository = new UserRepository()
  const userService = new UserService(userRepository)
  const manageJWT = new ManageJWT(config)
  const managePassword = new ManagePassword(config)
  const validateSignUpBody = new ValidateSignUpBody()
  const validateLoginBody = new ValidateLoginBody()
  const userController = new UserController(
    userService,
    validateSignUpBody,
    validateLoginBody,
    managePassword,
    manageJWT
  )
  test('userController is instanced correctly', async () => {
    expect(userController).toBeDefined()
  })

  test('userController create user return 200', async () => {
    const req = createRequest({
      body: {
        email: 'correo@correo.com',
        name: 'name',
        password: 'password',
      },
    })
    const res = createResponse()
    validateSignUpBody.validate = jest.fn().mockImplementationOnce(() => {
      return {}
    })
    userService.getUserByEmail = jest
      .fn()
      .mockImplementationOnce(() => undefined)
    managePassword.encrypt = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve('hashedPassword'))
    userService.create = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve({ id: 1 }))

    try {
      await userController.createUser(req, res)
      expect(res._getStatusCode()).toEqual(201)
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('userController create user return 500', async () => {
    const req = createRequest({
      body: {
        email: 'correo@correo.com',
        name: 'name',
        password: 'password',
      },
    })
    const res = createResponse()
    validateSignUpBody.validate = jest.fn().mockImplementationOnce(() =>
      Promise.resolve({
        result: {},
      })
    )
    userService.getUserByEmail = jest
      .fn()
      .mockImplementationOnce(() => Promise.reject(new Error('Mocked Error')))

    try {
      await userController.createUser(req, res)
    } catch (error) {
      expect(res._getStatusCode()).toEqual(500)
      expect(error).toBeDefined()
    }
  })

  test('userController create user return 400 bad request', async () => {
    const req = createRequest({
      body: {
        name: 'name',
        password: 'password',
      },
    })
    const res = createResponse()
    validateSignUpBody.validate = jest.fn().mockImplementationOnce(() => {
      return {
        error: 'Error',
      }
    })
    try {
      await userController.createUser(req, res)
    } catch (error) {
      expect(res._getStatusCode()).toEqual(400)
      expect(error).toBeDefined()
    }
  })

  test('userController create user return 400 user already signed', async () => {
    const req = createRequest({
      body: {
        email: 'correo@correo.com',
        name: 'name',
        password: 'password',
      },
    })
    const res = createResponse()
    validateSignUpBody.validate = jest.fn().mockImplementationOnce(() => {
      return {}
    })
    userService.getUserByEmail = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve({ id: 1 }))

    try {
      await userController.createUser(req, res)
      expect(res._getStatusCode()).toEqual(400)
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })
})

describe('charactersController', () => {
  beforeEach(() => {
    process.env.NODE_ENV = 'development'
    process.env.NODE_PORT = '4000'
    process.env.ROOT_PATH = '/app'
    process.env.DB_NAME = 'rickandmorty'
    process.env.DB_USER = 'admin'
    process.env.DB_HOST = 'localhost'
    process.env.DB_PORT = '5432'
    process.env.DB_DRIVER = 'postgres'
    process.env.DB_PASSWORD = 'admin'
    process.env.JWT_SECRET = 'rick@ndm0rty$3cr3t'
    process.env.SALT_ROUNDS = '10'
  })

  afterEach(() => {
    jest.clearAllMocks()
  })
  const config = new MockConfig()
  const userRepository = new UserRepository()
  const userService = new UserService(userRepository)
  const manageJWT = new ManageJWT(config)
  const managePassword = new ManagePassword(config)
  const validateSignUpBody = new ValidateSignUpBody()
  const validateLoginBody = new ValidateLoginBody()
  const userController = new UserController(
    userService,
    validateSignUpBody,
    validateLoginBody,
    managePassword,
    manageJWT
  )
  test('userController login return 200', async () => {
    const req = createRequest({
      body: {
        email: 'correo@correo.com',
        password: 'password',
      },
    })
    const res = createResponse()
    validateLoginBody.validate = jest.fn().mockImplementationOnce(() => {
      return {}
    })
    userService.getUserByEmail = jest.fn().mockImplementationOnce(() => {
      return { id: 1, name: 'Juan' }
    })
    managePassword.compare = jest.fn().mockImplementationOnce(() => true)
    manageJWT.sign = jest.fn().mockImplementationOnce(() => 'jsonwebtoken')

    try {
      await userController.login(req, res)
      expect(res._getStatusCode()).toEqual(200)
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('userController login return 500', async () => {
    const req = createRequest({
      body: {
        email: 'correo@correo.com',
        password: 'password',
      },
    })
    const res = createResponse()
    validateLoginBody.validate = jest.fn().mockImplementationOnce(() => {
      return {}
    })
    userService.getUserByEmail = jest.fn().mockImplementationOnce(() => {
      throw new Error('Mocked Error')
    })
    try {
      await userController.login(req, res)
    } catch (error) {
      expect(res._getStatusCode()).toEqual(500)
      expect(error).toBeDefined()
    }
  })

  test('userController login return 400 bad request', async () => {
    const req = createRequest({
      body: {
        password: 'password',
      },
    })
    const res = createResponse()
    validateLoginBody.validate = jest.fn().mockImplementationOnce(() => {
      return {
        error: {
          details: [
            {
              message: 'Email is required',
            },
          ],
        },
      }
    })
    try {
      await userController.login(req, res)
      expect(res._getStatusCode()).toEqual(400)
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('userController login return 404 user not found', async () => {
    const req = createRequest({
      body: {
        password: 'password',
      },
    })
    const res = createResponse()
    validateLoginBody.validate = jest.fn().mockImplementationOnce(() => {
      return {}
    })
    userService.getUserByEmail = jest
      .fn()
      .mockImplementationOnce(() => undefined)
    try {
      await userController.login(req, res)
      expect(res._getStatusCode()).toEqual(404)
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })

  test('userController login return 404 user not found', async () => {
    const req = createRequest({
      body: {
        password: 'password',
        email: 'correo@correo.com',
      },
    })
    const res = createResponse()
    validateLoginBody.validate = jest.fn().mockImplementationOnce(() => {
      return {}
    })
    userService.getUserByEmail = jest.fn().mockImplementationOnce(() => {
      return {
        id: 1,
        name: 'Juan',
        password:
          '$2a$10$HNivXoya0n/EXt/ImiO.QexXKJkTvbPO.f4Ce2xBVfDG7z6ribgca',
      }
    })
    managePassword.compare = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve(false))
    try {
      await userController.login(req, res)
      expect(res._getStatusCode()).toEqual(400)
    } catch (error) {
      expect(error).toBeUndefined()
    }
  })
})
