import * as express from 'express'
import { inject } from 'inversify'
import {
  controller,
  httpGet,
  httpPut,
  request,
  response,
} from 'inversify-express-utils'
import { TYPES } from '../../config/ioc/types'
import { ICharacterService } from '../services/character'
import { IManageJWT } from '../util'

@controller('/character')
export class CharacterController {
  constructor(
    @inject(TYPES.ICharacterService)
    private characterService: ICharacterService,
    @inject(TYPES.IManageJWT)
    private manageJWT: IManageJWT
  ) {}

  @httpPut('/add-to-favorite/:id')
  public async add(
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    try {
      if (req.params && !req.params.id) {
        throw new Error('The characterId is required')
      }

      if (!req.headers.authorization) {
        throw {
          message: 'Unauthorized',
          statusCode: 401,
        }
      }

      if (!this.authorize(req.headers.authorization, req.body)) {
        throw {
          message: 'Unauthorized',
          statusCode: 401,
        }
      }

      const result = await this.characterService.saveFavoriteCharacter(
        parseInt(req.params.id, 0)
      )

      return res
        .status(201)
        .json(result)
        .send()
    } catch (error: any) {
      return res
        .status(error.statusCode ? error.statusCode : 500)
        .json({ message: error.message })
        .send()
    }
  }

  @httpGet('/get-favorites')
  public async get(
    @request() req: express.Request,
    @response() res: express.Response
  ) {
    try {
      if (!req.headers.authorization) {
        throw {
          message: 'Unauthorized',
          statusCode: 401,
        }
      }

      if (!this.authorize(req.headers.authorization, req.body)) {
        throw {
          message: 'Unauthorized',
          statusCode: 401,
        }
      }

      const result = await this.characterService.getAllFavoriteCharacters(
        req.query
      )
      if (!result.length) {
        return res
          .status(400)
          .json({ message: 'No favorite characters found' })
          .send()
      }

      return res
        .status(200)
        .json(result)
        .send()
    } catch (error: any) {
      return res
        .status(error.statusCode ? error.statusCode : 500)
        .json({ message: error.message })
        .send()
    }
  }

  public authorize(token: string, body: any) {
    return this.manageJWT.decode(token, body.userId)
  }
}
