// tslint:disable-next-line: no-var-requires
require('module-alias/register')

// Permitir cargar variables de entorno desde .env para ambientes de development o testing
import dotenv = require('dotenv')
import morgan = require('morgan')

if (process.env.NODE_ENV !== 'production') {
  dotenv.config()
}

// Importar Reflect antes que Inversify
import 'reflect-metadata'

// Inicializar configuracion desde ambiente
import { ConfigService } from './config/vars/configService'
const configs = new ConfigService()
const configErr = configs.load()
if (configErr) throw new Error(configErr)

// Importar dependencias de Express y de Inversify
import bodyParser from 'body-parser'
import cors from 'cors'
import helmet from 'helmet'
import { InversifyExpressServer } from 'inversify-express-utils'
import { ConfigurationInput, createLightship, Lightship } from 'lightship'
import { container } from './config/ioc/inversify.config'

import { TYPES } from './config/ioc/types'

// Obtener variables de configuración desde entorno
const httpPort = configs.getVars().server.port
const httpRootPath = configs.getVars().server.rootPath

// Cargar las entidades inyectables
// la anotación @provide() las registra automaticamente
import './config/ioc/loader'
import init from './db/init'

// registrar instancia de config en container de dependencias
container.bind<any>(TYPES.IConfig).toConstantValue(configs)

// Configurar wrap de Express con Inversify para proveer inversión de control e inyección de dependencias
const server = new InversifyExpressServer(container, null, {
  rootPath: httpRootPath,
})

init()

server.setConfig(expressApp => {
  expressApp.use(
    bodyParser.urlencoded({
      extended: false,
    })
  )
  expressApp.use(bodyParser.json())
  expressApp.use(helmet())
  expressApp.use(cors())
  expressApp.use(morgan('dev'))
})
const app = server.build()
// Configurar Swagger-UI en ambientes de development y testing
import swaggerUiExpress from 'swagger-ui-express'
import apiDocsJson from './api-docs.json'

if (process.env.NODE_ENV !== 'production') {
  app.use(
    '/api-docs',
    swaggerUiExpress.serve,
    swaggerUiExpress.setup(apiDocsJson)
  )
}

const httpServer = app.listen(httpPort, () => {
  // tslint:disable-next-line: no-console
  console.log(
    `HTTP server started at http://localhost:${httpPort} succesfull!!!`
  )
})

exports = module.exports = app
