// Validaciones de variables de entorno de configuración del microservicio.
import joi from 'joi'

import { IConfig } from '.'

export class ConfigService implements IConfig {
  private vars: any

  constructor() {
    this.vars = undefined
  }

  /**
   * Retorna las variables de ambientes cargadas
   * @returns {any} Objeto con variables de ambiente
   */
  public getVars = () => {
    return this.vars
  }

  /**
   * Lee las variables de ambientes, valida su contenido y las carga en servicio de configuracion.
   *
   * @returns {string} Error - En caseo de ocurrir errores de validación, devuelve un mensaje de error, caso contrario retorna null.
   */
  public load = () => {
    const envVarsSchema = joi
      .object({
        NODE_ENV: joi
          .string()
          .valid('development')
          .required(),
        NODE_PORT: joi.number().required(),
        ROOT_PATH: joi.string().required(),
      })
      .unknown()
      .required()

    const validation = envVarsSchema.validate(process.env, {
      allowUnknown: true,
    })
    if (validation.error) {
      this.vars = undefined
      return `Config validation error: ${validation.error}`
    }
    const saltRound = String(process.env.SALT_ROUNDS)
    this.vars = {
      env: process.env.NODE_ENV,
      externalService: {
        url: process.env.EXTERNAL_SERVICE_URL,
      },
      isDev: process.env.NODE_ENV === 'development',
      jwtSecret: process.env.JWT_SECRET,
      saltRounds: parseInt(saltRound, 0),

      server: {
        port: Number(process.env.NODE_PORT),
        rootPath: process.env.ROOT_PATH,
      },
    }
    return null
  }
}
