import 'reflect-metadata'

const vars = {
  dbPort: 'DB_PORT',
  host: 'DB_HOST',
  name: 'DB_NAME',
  nodeEnv: 'NODE_ENV',
  nodePort: 'NODE_PORT',
  password: 'DB_PASSWORD',
  port: 'PORT',
  rootPath: 'ROOT_PATH',
  username: 'DB_USER',
  driver: 'DB_DRIVER',
  jwtSecret: 'JWT_SECRET',
  saltRound: 'SALT_ROUNDS',
  externalService: 'EXTERNAL_SERVICE_URL',
}

import { ConfigService } from './configService'

describe('ConfigService', () => {
  const config = new ConfigService()

  test('getVars devuelve undefined antes de llamar a load', () => {
    const env = config.getVars()
    expect(env).toBeUndefined()
  })

  test('load da error de validación si no estan configuradas las variables requeridas', () => {
    const err = config.load()
    expect(err).toContain('Config validation error:')
  })

  test('load NO da error de validación si estan configuradas las variables requeridas', () => {
    process.env[vars.nodeEnv] = 'development'
    process.env[vars.rootPath] = 'some-path'
    process.env[vars.dbPort] = '5432'
    process.env[vars.host] = 'localhost'
    process.env[vars.name] = 'dbname'
    process.env[vars.password] = 'pass'
    process.env[vars.username] = 'username'
    process.env[vars.nodePort] = '4000'
    process.env[vars.jwtSecret] = 'secret'
    process.env[vars.externalService] = 'http://page.com'
    process.env[vars.saltRound] = '10'
    process.env[vars.driver] = 'postgres'

    const err = config.load()
    expect(err).toBeFalsy()
    expect(config.getVars()).toBeTruthy()
  })

  test('load NO da error de validación si estan configuradas las variables requeridas conditional salt value', () => {
    process.env[vars.nodeEnv] = 'development'
    process.env[vars.rootPath] = 'some-path'
    process.env[vars.dbPort] = '5432'
    process.env[vars.host] = 'localhost'
    process.env[vars.name] = 'dbname'
    process.env[vars.password] = 'pass'
    process.env[vars.username] = 'username'
    process.env[vars.nodePort] = '4000'
    process.env[vars.jwtSecret] = 'secret'
    process.env[vars.externalService] = 'http://page.com'
    process.env[vars.driver] = 'postgres'

    const err = config.load()
    expect(err).toBeFalsy()
    expect(config.getVars()).toBeTruthy()
  })
})
