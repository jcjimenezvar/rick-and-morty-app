import { buildProviderModule, container } from "./inversify.config";

/* REST Controllers */
import "../../api/routes/charactersController";
import "../../api/routes/usersController";

/* Services */

import "../../api/services/character/characterService";
import "../../api/services/user/userService";

/* Repository */

import "../../db/dal/characters/character";
import "../../db/dal/users/user";

/* Utils */

import "../../api/util/manageJWT";
import "../../api/util/managePassword";
import "../../api/util/validateLoginBody";


/* Consumer */

import "../../api/consumer/axiosConsumer";

container.load(buildProviderModule());
