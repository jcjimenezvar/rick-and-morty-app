import { Container, inject, injectable } from 'inversify'
import { buildProviderModule, provide } from 'inversify-binding-decorators'
import { TYPES } from './types'

/* Services */

import { CharacterService } from '../../api/services/character/characterService'
import { UserService } from '../../api/services/user/userService'

/* Utils */

import { ManageJWT } from '../../api/util/manageJWT'
import { ManagePassword } from '../../api/util/managePassword'
import { ValidateLoginBody } from '../../api/util/validateLoginBody'
import { ValidateSignUpBody } from '../../api/util/validateSingUpBody'

/* Repository */

import { CharacterRepository } from '../../db/dal/characters/character'
import { UserRepository } from '../../db/dal/users/user'

/* Consumer */

import { AxiosConsumer } from '../../api/consumer/axiosConsumer'

const container = new Container()

container
  .bind<UserService>(TYPES.IUserService)
  .to(UserService)
  .inSingletonScope()

container
  .bind<UserRepository>(TYPES.IUserRepository)
  .to(UserRepository)
  .inSingletonScope()

container
  .bind<ValidateSignUpBody>(TYPES.IValidateSignUpBody)
  .to(ValidateSignUpBody)
  .inSingletonScope()

container
  .bind<ManagePassword>(TYPES.IManagePassword)
  .to(ManagePassword)
  .inSingletonScope()

container
  .bind<ValidateLoginBody>(TYPES.IValidateLoginBody)
  .to(ValidateLoginBody)
  .inSingletonScope()

container
  .bind<CharacterService>(TYPES.ICharacterService)
  .to(CharacterService)
  .inSingletonScope()

container
  .bind<CharacterRepository>(TYPES.ICharacterRepository)
  .to(CharacterRepository)
  .inSingletonScope()

container
  .bind<AxiosConsumer>(TYPES.IAxiosConsumer)
  .to(AxiosConsumer)
  .inSingletonScope()

  container
  .bind<ManageJWT>(TYPES.IManageJWT)
  .to(ManageJWT)
  .inSingletonScope()

export { buildProviderModule, container, provide, inject, injectable }
