import 'reflect-metadata'

import './loader'
import { container } from './inversify.config'

jest.mock('sequelize')

describe('ioc', () => {
  beforeEach(() => {
    process.env.NODE_ENV = 'development'
    process.env.NODE_PORT = '4000'
    process.env.ROOT_PATH = '/app'
    process.env.DB_NAME = 'rickandmorty'
    process.env.DB_USER = 'admin'
    process.env.DB_HOST = 'localhost'
    process.env.DB_PORT = '5432'
    process.env.DB_DRIVER = 'postgres'
    process.env.DB_PASSWORD = 'admin'
    process.env.JWT_SECRET = 'rick@ndm0rty$3cr3t'
    process.env.SALT_ROUNDS = '10'
  })
  test('Container de control debe estar definido', () => {
    expect(container).toBeTruthy()
  })
})
