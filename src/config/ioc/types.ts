export const TYPES = {
  IAxiosConsumer: Symbol.for("IAxiosConsumer"),
  ICharacterRepository: Symbol.for("ICharacterRepository"),
  ICharacterService: Symbol.for("ICharacterService"),
  IConfig: Symbol.for("IConfig"),
  IManageJWT: Symbol.for("IManageJWT"),
  IManagePassword: Symbol.for("IManagePassword"),
  IUserRepository: Symbol.for("IUserRepository"),
  IUserService: Symbol.for("IUserService"),
  IValidateLoginBody: Symbol.for("IValidateLoginBody"),
  IValidateSignUpBody: Symbol.for("IValidateSignUpBody"),
};
