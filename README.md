# Rick & Morty

Esta es una aplicación que permite listar los personajes de Rick y Morty cosumiendo el endpoint público [RickAndmorty](https://rickandmortyapi.com). El proyecto esta desarrollado con Typescript, base de datos PostgreSql usando el ORM Sequelize, Inyección de dependencias. 

## Requisitos
- Instalar NodeJs en su latest versión [NodeJs](https://nodejs.org/en/download/)
- Instalar Docker [Docker](https://www.docker.com/products/docker-desktop/)
- Instalar docker-compose [docker-compose](https://docs.docker.com/compose/install/)

## Usage

Para iniciar el proyecto se deben tener en cuenta los siguientes pasos:

- Seleccionar una ruta donde clonar el proyecto
- Clonar el [repositorio](https://gitlab.com/jcjimenezvar/rick-and-morty-app.git) público utilizando `git clone https://gitlab.com/jcjimenezvar/rick-and-morty-app.git`
- Una vez clonado utilizando la consola se debe ubicar en la carpeta y utilizar el comando `cd rick-and-morty-app` y luego `code .` esto abrirá el editor de código donde veremos la estructura de carpetas.

### Ejecutar el proyecto
El proyecto esta configurado para poder ser desplegado de dos formas, usando `docker-compose` simplemente ejecutando el comando `docker-compose up -d o docker-compose up` este ultimo si queremos ver los `logs` que va dejando la aplicación en su despliegue. La otra forma es la siguiente:
- Ejecutar la base de datos:

Ubicar el siguiente archivo en una ruta a la cual se pueda acceder fácilmente con la terminal, el archivo puede llamarse `docker-compose.yml`
```
version: '3'

services:
  postgres:
    image: postgres:13.1
    healthcheck:
      test: [ "CMD", "pg_isready", "-q", "-d", "postgres", "-U", "root" ]
      timeout: 45s
      interval: 10s
      retries: 10
    restart: always
    environment:
      - POSTGRES_USER=admin
      - POSTGRES_PASSWORD=admin
      - POSTGRES_PORT=5432
      - POSTGRES_DB=rickandmorty
      
    volumes:
      - ./db:/docker-entrypoint-initdb.d/
    ports:
      - 5432:5432
```
- En la carpeta donde esta ubicado el archivo ejecutar el siguiente comando `docker-compose up -d (sin logs) o docker-compose up (con logs)` 
- El paso anterior creara localmente una imagen de postgres en Docker
- Ahora vamos a la terminal de visual studio code y ejecutamos el siguiente comando `npm run build` esto realizara diferentes procesos, inicialmente valida la estructura de código con respecto a `lint`, luego compila el codigo Typescript a Javascript y ejecuta el servidor para este caso usamos *ExpressJs*
- El servidor se inicia en el `http://localhost:4000/app/`

## Documentation

El proyecto tiene implementado [swagger](https://swagger.io/) para visualizar la estructura de los request accediendo a la siguiente [url](http://localhost:4000/api-docs), de forma adicional en el proyecto se encuentra la colección de postman para poder ejecutar las pruebas.

## Pruebas unitarias

Para ejecutar las pruebas unitarias, se debe ingresar a la terminal y ejecutar el comando `npm run test`

## Variables de entorno
El proyecto cuenta con un archivo de configuración llamado .env-example
```
NODE_ENV=development
NODE_PORT=4000
ROOT_PATH=/app
DB_NAME=rickandmorty
DB_USER=admin
DB_HOST=localhost
DB_PORT=5432
DB_DRIVER=postgres
DB_PASSWORD=admin
JWT_SECRET=rick@ndm0rty$3cr3t
SALT_ROUNDS=10
EXTERNAL_SERVICE_URL=https://rickandmortyapi.com/api/character
```
## Dependencias

Algunas de las dependencias usadas en el proyecto son:
```
    "axios": "Utilizado para hacer peticiones http ", 
    "bcrypt": "Se uso para encriptar la contraseña del usuario antes de guardarla en la base de datos",
    "body-parser": "Para convertir el request",
    "dotenv": "Cargar las variables de entorno",
    "express": "Servidor",
    "inversify": "Inyeccion de dependencias",
    "joi": "Validador de payload",
    "jsonwebtoken": "Generar el JWT para autenticar las peticiones",
    "node-mocks-http": "mockear request y response",
    "pg": "driver para conectar la bd",
    "reflect-metadata": "",
    "sequelize": "ORM para modelar la BD",
    "swagger-ui-express": "API Docs"
```
